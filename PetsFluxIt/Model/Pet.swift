//
//  Pet.swift
//  PetsFluxIt
//
//  Created by Ale on 19/11/16.
//  Copyright © 2016 Alejandro Zalazar. All rights reserved.
//

import Foundation
import ObjectMapper

class Pet: Mappable{
    var id:Int?
    var name:String?
    var status:String?
    var category:Category?
    var photos:[String]?
    var tags:[Tag]?
    
    required init?(map: Map) {
    }
    
    // Mappable
    func mapping(map: Map) {
        id    <- map["id"]
        name         <- map["name"]
        status      <- map["status"]
        category      <- map["category"]
        photos <- map["photoUrls"]
        tags <- map["tags"]
    }
}
