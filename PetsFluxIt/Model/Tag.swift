//
//  Tag.swift
//  PetsFluxIt
//
//  Created by Ale on 19/11/16.
//  Copyright © 2016 Alejandro Zalazar. All rights reserved.
//

import Foundation
import ObjectMapper

class Tag:Mappable{
    var id:Int?
    var name:String?
    
    required init?(map: Map) {
    }
    
    // Mappable
    func mapping(map: Map) {
        id    <- map["id"]
        name         <- map["name"]
    }
}
