//
//  PhotosTableViewController.swift
//  PetsFluxIt
//
//  Created by Ale on 20/11/16.
//  Copyright © 2016 Alejandro Zalazar. All rights reserved.
//

import UIKit

class PhotosTableViewController: UITableViewController,ButtonCellDelegate {
    
    var currentPet:Pet?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let parentController = self.tabBarController as? DetailScreenViewController
        currentPet = parentController?.currentPet
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func cellPhotoTapped(cell: PhotoTableViewCell) {
        var message = "PhotoUrl : " + (currentPet?.photos?[tableView.indexPath(for: cell)!.row])!
        
        let alert = UIAlertController(title: "Detail", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)

    }
    
    func cellTapped(cell: ProfileTableViewCell) {
    
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (currentPet?.photos?.count)!
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PhotoTableViewCell", for: indexPath) as! PhotoTableViewCell
        
        cell.photoUrl.text = currentPet?.photos?[indexPath.row]
        
        if cell.buttonDelegate == nil{
            cell.buttonDelegate = self
        }

        return cell
    }
 
}
