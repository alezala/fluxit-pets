//
//  HomeTableViewController.swift
//  PetsFluxIt
//
//  Created by Ale on 17/11/16.
//  Copyright © 2016 Alejandro Zalazar. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import SwiftSpinner

class HomeTableViewController: UITableViewController {
    
    var pets:[Pet] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        SwiftSpinner.show("Loading Pets...")
        
        Alamofire.request("http://petstore.swagger.io/v2/pet/findByStatus?status=available").responseArray { (response: DataResponse<[Pet]>) in
            
            self.pets = response.result.value!
            self.tableView.reloadData()
            
            SwiftSpinner.hide()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "petDetail" ,
            let nextScene = segue.destination as? DetailScreenViewController ,
            let indexPath = self.tableView.indexPathForSelectedRow {
            let selectedPet = pets[indexPath.row]
            nextScene.currentPet = selectedPet
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.pets.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PetTableViewCell", for: indexPath) as! PetTableViewCell
        
        let pet = self.pets[indexPath.row]
        
        cell.petName.text = pet.name
        cell.petCategory.text = pet.category?.name

        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
