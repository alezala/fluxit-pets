//
//  DetailScreenViewController.swift
//  PetsFluxIt
//
//  Created by Ale on 20/11/16.
//  Copyright © 2016 Alejandro Zalazar. All rights reserved.
//

import UIKit

class DetailScreenViewController: UITabBarController {
    
    var currentPet:Pet?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = currentPet?.name
        
        if self.currentPet?.photos?.count == 0{
            viewControllers?.remove(at: 1)
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    

}
