//
//  ProfileTableViewController.swift
//  PetsFluxIt
//
//  Created by Ale on 20/11/16.
//  Copyright © 2016 Alejandro Zalazar. All rights reserved.
//

import UIKit

class ProfileTableViewController: UITableViewController,ButtonCellDelegate {

    var currentPet:Pet?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let parentController = self.tabBarController as? DetailScreenViewController
        currentPet = parentController?.currentPet
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()    }
    

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 4
    }
    
    func cellPhotoTapped(cell: PhotoTableViewCell) {
        
    }

    func cellTapped(cell: ProfileTableViewCell) {
        
        var message = ""
        switch tableView.indexPath(for: cell)!.row {
        case 0:
            message = "name : "+(currentPet?.name)!
            break
        case 1:
            message = "category : "+(currentPet?.category?.name)!
            break
        case 2:
            var tagsText = "";
            if (currentPet?.tags?.count)! > 0{
                for tag in (currentPet?.tags)!{
                    tagsText+=tag.name!+" "
                }
            }else{
                tagsText = "No tags"
            }
            message = "tags : "+tagsText
            break
        case 3:
            message = "status : "+(currentPet?.status!)!
            break
        default:
            print("No data")
        }
        
        let alert = UIAlertController(title: "Detail", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)

        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileTableViewCell", for: indexPath) as! ProfileTableViewCell
        
        
        switch  indexPath.row{
        case 0:
            cell.itemTxt.text = "Name"
            cell.valueTxt.text = currentPet?.name
            break
        case 1 :
            cell.itemTxt.text = "Category"
            cell.valueTxt.text = currentPet?.category?.name
            break
        case 2:
            cell.itemTxt.text = "Tags"
            var tagsText = "";
            if (currentPet?.tags?.count)! > 0{
                for tag in (currentPet?.tags)!{
                    tagsText+=tag.name!+" "
                }
            }else{
                tagsText = "No tags"
            }
            
            cell.valueTxt.text = tagsText
            break
        case 3:
            cell.itemTxt.text = "Status"
            cell.valueTxt.text = currentPet?.status
            break
        default:
            cell.itemTxt.text = "Status"
            cell.valueTxt.text = currentPet?.status
            break
        }
        
        if cell.buttonDelegate == nil{
            cell.buttonDelegate = self
        }
        
        
        return cell
    }
    
}
