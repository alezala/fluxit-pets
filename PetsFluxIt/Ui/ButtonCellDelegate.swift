//
//  ButtonCellDelegate.swift
//  PetsFluxIt
//
//  Created by Ale on 20/11/16.
//  Copyright © 2016 Alejandro Zalazar. All rights reserved.
//

import Foundation

protocol ButtonCellDelegate {
    func cellTapped(cell: ProfileTableViewCell)
    func cellPhotoTapped(cell: PhotoTableViewCell)
}
