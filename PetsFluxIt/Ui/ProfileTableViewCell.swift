//
//  ProfileTableViewCell.swift
//  PetsFluxIt
//
//  Created by Ale on 20/11/16.
//  Copyright © 2016 Alejandro Zalazar. All rights reserved.
//

import UIKit

class ProfileTableViewCell: UITableViewCell {
    
    @IBOutlet weak var itemTxt: UILabel!
    @IBOutlet weak var valueTxt: UILabel!
    @IBOutlet weak var seeButton: UIButton!
    
    var buttonDelegate: ButtonCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func openDetail(_ sender: UIButton) {
        
        if let delegate = buttonDelegate {
            delegate.cellTapped(cell: self)
        }
            
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
