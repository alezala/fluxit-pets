//
//  PhotoTableViewCell.swift
//  PetsFluxIt
//
//  Created by Ale on 20/11/16.
//  Copyright © 2016 Alejandro Zalazar. All rights reserved.
//

import UIKit

class PhotoTableViewCell: UITableViewCell {
    
    var buttonDelegate: ButtonCellDelegate?
    
    
    @IBOutlet weak var photoUrl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func openDetail(_ sender: UIButton) {
        
        if let delegate = buttonDelegate {
            delegate.cellPhotoTapped(cell: self)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
